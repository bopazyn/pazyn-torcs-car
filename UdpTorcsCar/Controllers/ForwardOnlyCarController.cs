﻿using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers
{
    public class ForwardOnlyCarController : ICarControllerable
    {
        public void Drive( CarEfectorCollection effectors, CarSensorCollection sensors )
        {
            effectors.Gear.Value = 1;
            effectors.Accel.Value = 1;
        }
    }
}