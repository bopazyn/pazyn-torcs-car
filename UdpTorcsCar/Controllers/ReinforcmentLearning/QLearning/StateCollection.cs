﻿using System.Collections.Generic;
using System.Linq;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning
{
    public class StateCollection
    {
        public List<State> States { get; set; } = new List<State>();

        public State GetState( CarEfectorCollection effectors, CarSensorCollection sensors )
        {
            State newState = new State(effectors, sensors);
            State state = States.FirstOrDefault(x => State.Equals(x, newState));
            if (state != null)
                return state;
            state = newState;
            States.Add(state);
            return state;
        }
    }
}
