﻿namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.UI
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TemperatureTextBox = new System.Windows.Forms.TextBox();
            this.GammaTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AngleReinforcementTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SpeedReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SpeedSumReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BreakReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.AccelReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SpeedSumBelowOneReiforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SpeedToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.AngleToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.DistanceFromStartToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TrackToleranceTectBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.FocusToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SteeringToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.BreakToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.AccelToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.TrackPositionReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TrackPositionToleranceTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.OffTrackReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.TraveledDistanceReinforcmentTextBox = new System.Windows.Forms.TextBox();
            this.TraveledDistanceReinforcment = new System.Windows.Forms.Label();
            this.SpeedComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.AngleComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.AccelComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.BreakComparasionEnabledCkechBox = new System.Windows.Forms.CheckBox();
            this.SteeringComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.FocusComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.TrackComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.DistanceFromStartComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.TrackPositionComparasionEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Temperature";
            // 
            // TemperatureTextBox
            // 
            this.TemperatureTextBox.Location = new System.Drawing.Point(187, 6);
            this.TemperatureTextBox.Name = "TemperatureTextBox";
            this.TemperatureTextBox.Size = new System.Drawing.Size(249, 20);
            this.TemperatureTextBox.TabIndex = 1;
            // 
            // GammaTextBox
            // 
            this.GammaTextBox.Location = new System.Drawing.Point(187, 32);
            this.GammaTextBox.Name = "GammaTextBox";
            this.GammaTextBox.Size = new System.Drawing.Size(249, 20);
            this.GammaTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Gamma";
            // 
            // AngleReinforcementTextBox
            // 
            this.AngleReinforcementTextBox.Location = new System.Drawing.Point(187, 73);
            this.AngleReinforcementTextBox.Name = "AngleReinforcementTextBox";
            this.AngleReinforcementTextBox.Size = new System.Drawing.Size(249, 20);
            this.AngleReinforcementTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "AngleReinforcement";
            // 
            // SpeedReinforcmentTextBox
            // 
            this.SpeedReinforcmentTextBox.Location = new System.Drawing.Point(187, 99);
            this.SpeedReinforcmentTextBox.Name = "SpeedReinforcmentTextBox";
            this.SpeedReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.SpeedReinforcmentTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "SpeedReinforcment";
            // 
            // SpeedSumReinforcmentTextBox
            // 
            this.SpeedSumReinforcmentTextBox.Location = new System.Drawing.Point(187, 125);
            this.SpeedSumReinforcmentTextBox.Name = "SpeedSumReinforcmentTextBox";
            this.SpeedSumReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.SpeedSumReinforcmentTextBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "SpeedSumReinforcment";
            // 
            // BreakReinforcmentTextBox
            // 
            this.BreakReinforcmentTextBox.Location = new System.Drawing.Point(187, 151);
            this.BreakReinforcmentTextBox.Name = "BreakReinforcmentTextBox";
            this.BreakReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.BreakReinforcmentTextBox.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "BreakReinforcment";
            // 
            // AccelReinforcmentTextBox
            // 
            this.AccelReinforcmentTextBox.Location = new System.Drawing.Point(187, 177);
            this.AccelReinforcmentTextBox.Name = "AccelReinforcmentTextBox";
            this.AccelReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.AccelReinforcmentTextBox.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "AccelReinforcment";
            // 
            // SpeedSumBelowOneReiforcmentTextBox
            // 
            this.SpeedSumBelowOneReiforcmentTextBox.Location = new System.Drawing.Point(187, 203);
            this.SpeedSumBelowOneReiforcmentTextBox.Name = "SpeedSumBelowOneReiforcmentTextBox";
            this.SpeedSumBelowOneReiforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.SpeedSumBelowOneReiforcmentTextBox.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "SpeedSumBelowOneReiforcment";
            // 
            // SpeedToleranceTextBox
            // 
            this.SpeedToleranceTextBox.Location = new System.Drawing.Point(187, 358);
            this.SpeedToleranceTextBox.Name = "SpeedToleranceTextBox";
            this.SpeedToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.SpeedToleranceTextBox.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 361);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "SpeedTolerance";
            // 
            // AngleToleranceTextBox
            // 
            this.AngleToleranceTextBox.Location = new System.Drawing.Point(187, 384);
            this.AngleToleranceTextBox.Name = "AngleToleranceTextBox";
            this.AngleToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.AngleToleranceTextBox.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 387);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "AngleTolerance";
            // 
            // DistanceFromStartToleranceTextBox
            // 
            this.DistanceFromStartToleranceTextBox.Location = new System.Drawing.Point(187, 540);
            this.DistanceFromStartToleranceTextBox.Name = "DistanceFromStartToleranceTextBox";
            this.DistanceFromStartToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.DistanceFromStartToleranceTextBox.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 543);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(142, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "DistanceFromStartTolerance";
            // 
            // TrackToleranceTectBox
            // 
            this.TrackToleranceTectBox.Location = new System.Drawing.Point(187, 514);
            this.TrackToleranceTectBox.Name = "TrackToleranceTectBox";
            this.TrackToleranceTectBox.Size = new System.Drawing.Size(249, 20);
            this.TrackToleranceTectBox.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 517);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "TrackTolerance";
            // 
            // FocusToleranceTextBox
            // 
            this.FocusToleranceTextBox.Location = new System.Drawing.Point(187, 488);
            this.FocusToleranceTextBox.Name = "FocusToleranceTextBox";
            this.FocusToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.FocusToleranceTextBox.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 491);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "FocusTolerance";
            // 
            // SteeringToleranceTextBox
            // 
            this.SteeringToleranceTextBox.Location = new System.Drawing.Point(187, 462);
            this.SteeringToleranceTextBox.Name = "SteeringToleranceTextBox";
            this.SteeringToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.SteeringToleranceTextBox.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 465);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "SteeringTolerance";
            // 
            // BreakToleranceTextBox
            // 
            this.BreakToleranceTextBox.Location = new System.Drawing.Point(187, 436);
            this.BreakToleranceTextBox.Name = "BreakToleranceTextBox";
            this.BreakToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.BreakToleranceTextBox.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 439);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "BreakTolerance";
            // 
            // AccelToleranceTextBox
            // 
            this.AccelToleranceTextBox.Location = new System.Drawing.Point(187, 410);
            this.AccelToleranceTextBox.Name = "AccelToleranceTextBox";
            this.AccelToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.AccelToleranceTextBox.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 413);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "AccelTolerance";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(366, 598);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 32;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(285, 598);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 33;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // TrackPositionReinforcmentTextBox
            // 
            this.TrackPositionReinforcmentTextBox.Location = new System.Drawing.Point(187, 229);
            this.TrackPositionReinforcmentTextBox.Name = "TrackPositionReinforcmentTextBox";
            this.TrackPositionReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.TrackPositionReinforcmentTextBox.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 232);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(135, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "TrackPositionReinforcment";
            // 
            // TrackPositionToleranceTextBox
            // 
            this.TrackPositionToleranceTextBox.Location = new System.Drawing.Point(187, 566);
            this.TrackPositionToleranceTextBox.Name = "TrackPositionToleranceTextBox";
            this.TrackPositionToleranceTextBox.Size = new System.Drawing.Size(249, 20);
            this.TrackPositionToleranceTextBox.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 569);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "TrackPositionTolerance";
            // 
            // OffTrackReinforcmentTextBox
            // 
            this.OffTrackReinforcmentTextBox.Location = new System.Drawing.Point(187, 255);
            this.OffTrackReinforcmentTextBox.Name = "OffTrackReinforcmentTextBox";
            this.OffTrackReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.OffTrackReinforcmentTextBox.TabIndex = 39;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 258);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "OffTrackReinforcment";
            // 
            // TraveledDistanceReinforcmentTextBox
            // 
            this.TraveledDistanceReinforcmentTextBox.Location = new System.Drawing.Point(187, 281);
            this.TraveledDistanceReinforcmentTextBox.Name = "TraveledDistanceReinforcmentTextBox";
            this.TraveledDistanceReinforcmentTextBox.Size = new System.Drawing.Size(249, 20);
            this.TraveledDistanceReinforcmentTextBox.TabIndex = 41;
            // 
            // TraveledDistanceReinforcment
            // 
            this.TraveledDistanceReinforcment.AutoSize = true;
            this.TraveledDistanceReinforcment.Location = new System.Drawing.Point(12, 284);
            this.TraveledDistanceReinforcment.Name = "TraveledDistanceReinforcment";
            this.TraveledDistanceReinforcment.Size = new System.Drawing.Size(154, 13);
            this.TraveledDistanceReinforcment.TabIndex = 40;
            this.TraveledDistanceReinforcment.Text = "TraveledDistanceReinforcment";
            // 
            // checkBox1
            // 
            this.SpeedComparasionEnabledCheckBox.AutoSize = true;
            this.SpeedComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 360);
            this.SpeedComparasionEnabledCheckBox.Name = "SpeedComparasionEnabledCheckBox";
            this.SpeedComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.SpeedComparasionEnabledCheckBox.TabIndex = 42;
            this.SpeedComparasionEnabledCheckBox.Text = "Enabled";
            this.SpeedComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.AngleComparasionEnabledCheckBox.AutoSize = true;
            this.AngleComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 390);
            this.AngleComparasionEnabledCheckBox.Name = "AngleComparasionEnabledCheckBox";
            this.AngleComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.AngleComparasionEnabledCheckBox.TabIndex = 43;
            this.AngleComparasionEnabledCheckBox.Text = "Enabled";
            this.AngleComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.AccelComparasionEnabledCheckBox.AutoSize = true;
            this.AccelComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 413);
            this.AccelComparasionEnabledCheckBox.Name = "AccelComparasionEnabledCheckBox";
            this.AccelComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.AccelComparasionEnabledCheckBox.TabIndex = 44;
            this.AccelComparasionEnabledCheckBox.Text = "Enabled";
            this.AccelComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.BreakComparasionEnabledCkechBox.AutoSize = true;
            this.BreakComparasionEnabledCkechBox.Location = new System.Drawing.Point(442, 439);
            this.BreakComparasionEnabledCkechBox.Name = "BreakComparasionEnabledCkechBox";
            this.BreakComparasionEnabledCkechBox.Size = new System.Drawing.Size(65, 17);
            this.BreakComparasionEnabledCkechBox.TabIndex = 45;
            this.BreakComparasionEnabledCkechBox.Text = "Enabled";
            this.BreakComparasionEnabledCkechBox.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.SteeringComparasionEnabledCheckBox.AutoSize = true;
            this.SteeringComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 465);
            this.SteeringComparasionEnabledCheckBox.Name = "SteeringComparasionEnabledCheckBox";
            this.SteeringComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.SteeringComparasionEnabledCheckBox.TabIndex = 46;
            this.SteeringComparasionEnabledCheckBox.Text = "Enabled";
            this.SteeringComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.FocusComparasionEnabledCheckBox.AutoSize = true;
            this.FocusComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 491);
            this.FocusComparasionEnabledCheckBox.Name = "FocusComparasionEnabledCheckBox";
            this.FocusComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.FocusComparasionEnabledCheckBox.TabIndex = 47;
            this.FocusComparasionEnabledCheckBox.Text = "Enabled";
            this.FocusComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.TrackComparasionEnabledCheckBox.AutoSize = true;
            this.TrackComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 517);
            this.TrackComparasionEnabledCheckBox.Name = "TrackComparasionEnabledCheckBox";
            this.TrackComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.TrackComparasionEnabledCheckBox.TabIndex = 48;
            this.TrackComparasionEnabledCheckBox.Text = "Enabled";
            this.TrackComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.DistanceFromStartComparasionEnabledCheckBox.AutoSize = true;
            this.DistanceFromStartComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 543);
            this.DistanceFromStartComparasionEnabledCheckBox.Name = "DistanceFromStartComparasionEnabledCheckBox";
            this.DistanceFromStartComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.DistanceFromStartComparasionEnabledCheckBox.TabIndex = 49;
            this.DistanceFromStartComparasionEnabledCheckBox.Text = "Enabled";
            this.DistanceFromStartComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.TrackPositionComparasionEnabledCheckBox.AutoSize = true;
            this.TrackPositionComparasionEnabledCheckBox.Location = new System.Drawing.Point(442, 569);
            this.TrackPositionComparasionEnabledCheckBox.Name = "TrackPositionComparasionEnabledCheckBox";
            this.TrackPositionComparasionEnabledCheckBox.Size = new System.Drawing.Size(65, 17);
            this.TrackPositionComparasionEnabledCheckBox.TabIndex = 50;
            this.TrackPositionComparasionEnabledCheckBox.Text = "Enabled";
            this.TrackPositionComparasionEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 640);
            this.Controls.Add(this.TrackPositionComparasionEnabledCheckBox);
            this.Controls.Add(this.DistanceFromStartComparasionEnabledCheckBox);
            this.Controls.Add(this.TrackComparasionEnabledCheckBox);
            this.Controls.Add(this.FocusComparasionEnabledCheckBox);
            this.Controls.Add(this.SteeringComparasionEnabledCheckBox);
            this.Controls.Add(this.BreakComparasionEnabledCkechBox);
            this.Controls.Add(this.AccelComparasionEnabledCheckBox);
            this.Controls.Add(this.AngleComparasionEnabledCheckBox);
            this.Controls.Add(this.SpeedComparasionEnabledCheckBox);
            this.Controls.Add(this.TraveledDistanceReinforcmentTextBox);
            this.Controls.Add(this.TraveledDistanceReinforcment);
            this.Controls.Add(this.OffTrackReinforcmentTextBox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.TrackPositionToleranceTextBox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.TrackPositionReinforcmentTextBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.DistanceFromStartToleranceTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TrackToleranceTectBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.FocusToleranceTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.SteeringToleranceTextBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.BreakToleranceTextBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.AccelToleranceTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.AngleToleranceTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.SpeedToleranceTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.SpeedSumBelowOneReiforcmentTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.AccelReinforcmentTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BreakReinforcmentTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.SpeedSumReinforcmentTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SpeedReinforcmentTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AngleReinforcementTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GammaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TemperatureTextBox);
            this.Controls.Add(this.label1);
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TemperatureTextBox;
        private System.Windows.Forms.TextBox GammaTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AngleReinforcementTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox SpeedReinforcmentTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SpeedSumReinforcmentTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox BreakReinforcmentTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox AccelReinforcmentTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SpeedSumBelowOneReiforcmentTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SpeedToleranceTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox AngleToleranceTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox DistanceFromStartToleranceTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TrackToleranceTectBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox FocusToleranceTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox SteeringToleranceTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox BreakToleranceTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox AccelToleranceTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button SaveButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.TextBox TrackPositionReinforcmentTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TrackPositionToleranceTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox OffTrackReinforcmentTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TraveledDistanceReinforcmentTextBox;
        private System.Windows.Forms.Label TraveledDistanceReinforcment;
        private System.Windows.Forms.CheckBox SpeedComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox AngleComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox AccelComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox BreakComparasionEnabledCkechBox;
        private System.Windows.Forms.CheckBox SteeringComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox FocusComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox TrackComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox DistanceFromStartComparasionEnabledCheckBox;
        private System.Windows.Forms.CheckBox TrackPositionComparasionEnabledCheckBox;
    }
}