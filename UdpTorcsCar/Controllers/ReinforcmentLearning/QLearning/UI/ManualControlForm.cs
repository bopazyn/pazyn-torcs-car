﻿using System;
using System.Windows.Forms;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.UI
{
    public partial class ManualControlForm : Form
    {
        private QLerningCarController QLerningCarController { get; set; }
        private Boolean[] KeyState { get; set;  } = new Boolean[4];
        public ManualControlForm( QLerningCarController controller)
        {
            QLerningCarController = controller;
            QLerningCarController.IsManualEnabled = true;
            QLerningCarController.GetManualControlActioCallback  += GetManualControlActioCallback;
            InitializeComponent();
        }

        private Action GetManualControlActioCallback()
        {
            Int32 x = 0;
            Int32 y = 0;
            if (KeyState[3])
                x += 1000;
            if (KeyState[1])
                x -= 1000;
            if (KeyState[0])
                y += 1000;
            if (KeyState[2])
                y -= 1000;
            Action action = new Action(x,y);
            return action;
        }

        private void CloseButton_Click( Object sender, EventArgs e )
        {
            QLerningCarController.IsManualEnabled = false;
            Close();
        }

        protected override Boolean ProcessCmdKey( ref Message msg, Keys keyData )
        {
            if (keyData == Keys.Up)
                KeyState[0] = true;
            if (keyData == Keys.Right)
                KeyState[1] = true;
            if (keyData == Keys.Down)
                KeyState[2] = true;
            if (keyData == Keys.Left)
                KeyState[3] = true;

            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void ManualControlForm_KeyUp( Object sender, KeyEventArgs e )
        {
            if (e.KeyData == Keys.Up)
                KeyState[0] = false;
            if (e.KeyData == Keys.Right)
                KeyState[1] = false;
            if (e.KeyData == Keys.Down)
                KeyState[2] = false;
            if (e.KeyData == Keys.Left)
                KeyState[3] = false;
        }
    }
}
