﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.UI
{
    public partial class ConfigurationForm : Form
    {
        public ConfigurationForm()
        {
            Configuration = QLearning.Configuration.Configuration.Load();

            InitializeComponent();
        }

        private Configuration.Configuration Configuration { get; }

        private void ConfigurationForm_Load( Object sender, EventArgs e )
        {
            FillControls();
        }

        private void FillControls()
        {
            TemperatureTextBox.Text = Configuration.Temperature.ToString(CultureInfo.CurrentCulture);
            GammaTextBox.Text = Configuration.Gamma.ToString(CultureInfo.CurrentCulture);
            AngleReinforcementTextBox.Text = Configuration.AngleReinforcement.ToString(CultureInfo.CurrentCulture);
            SpeedReinforcmentTextBox.Text = Configuration.SpeedReinforcment.ToString(CultureInfo.CurrentCulture);
            SpeedSumReinforcmentTextBox.Text = Configuration.SpeedSumReinforcment.ToString(CultureInfo.CurrentCulture);
            BreakReinforcmentTextBox.Text = Configuration.BreakReinforcment.ToString(CultureInfo.CurrentCulture);
            AccelReinforcmentTextBox.Text = Configuration.AccelReinforcment.ToString(CultureInfo.CurrentCulture);
            SpeedSumBelowOneReiforcmentTextBox.Text = Configuration.SpeedSumBelowOneReiforcment.ToString(CultureInfo.CurrentCulture);
            TrackPositionReinforcmentTextBox.Text = Configuration.TrackPositionReinforcment.ToString(CultureInfo.CurrentCulture);
            OffTrackReinforcmentTextBox.Text = Configuration.OffTrackReinforcment.ToString(CultureInfo.CurrentCulture);
            TraveledDistanceReinforcmentTextBox.Text = Configuration.TraveledDistanceReinforcment.ToString(CultureInfo.CurrentCulture);

            SpeedToleranceTextBox.Text = Configuration.SpeedTolerance.ToString(CultureInfo.CurrentCulture);
            AngleToleranceTextBox.Text = Configuration.AngleTolerance.ToString(CultureInfo.CurrentCulture);
            AccelToleranceTextBox.Text = Configuration.AccelTolerance.ToString(CultureInfo.CurrentCulture);
            BreakToleranceTextBox.Text = Configuration.BreakTolerance.ToString(CultureInfo.CurrentCulture);
            SteeringToleranceTextBox.Text = Configuration.SteeringTolerance.ToString(CultureInfo.CurrentCulture);
            FocusToleranceTextBox.Text = Configuration.FocusTolerance.ToString(CultureInfo.CurrentCulture);
            TrackToleranceTectBox.Text = Configuration.TrackTolerance.ToString(CultureInfo.CurrentCulture);
            DistanceFromStartToleranceTextBox.Text = Configuration.DistanceFromStartTolerance.ToString(CultureInfo.CurrentCulture);
            TrackPositionToleranceTextBox.Text = Configuration.TrackPositionTolerance.ToString(CultureInfo.CurrentCulture);

            SpeedComparasionEnabledCheckBox.Checked = Configuration.SpeedComparasionEnabled;
            AngleComparasionEnabledCheckBox.Checked = Configuration.AngleComparasionEnabled;
            AccelComparasionEnabledCheckBox.Checked = Configuration.AccelComparasionEnabled = Configuration.AccelComparasionEnabled;
            BreakComparasionEnabledCkechBox.Checked = Configuration.BreakComparasionEnabled;
            SpeedComparasionEnabledCheckBox.Checked = Configuration.SteeringComparasionEnabled;
            FocusComparasionEnabledCheckBox.Checked = Configuration.FocusComparasionEnabled;
            TrackComparasionEnabledCheckBox.Checked = Configuration.TrackComparasionEnabled;
            DistanceFromStartComparasionEnabledCheckBox.Checked = Configuration.DistanceFromStartComparasionEnabled;
            TrackPositionComparasionEnabledCheckBox.Checked = Configuration.TrackPositionComparasionEnabled;
        }

        private void SaveButton_Click( Object sender, EventArgs e )
        {
            CollectData();
            QLearning.Configuration.Configuration.Update(Configuration);
            Configuration.Save();
            SaveButton.DialogResult = DialogResult.OK;
            Close();
        }

        private void CollectData()
        {
            Configuration.Temperature = Double.Parse(TemperatureTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.Gamma = Double.Parse(GammaTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.AngleReinforcement = Double.Parse(AngleReinforcementTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.SpeedReinforcment = Double.Parse(SpeedReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.SpeedSumReinforcment = Double.Parse(SpeedSumReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.BreakReinforcment = Double.Parse(BreakReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.AccelReinforcment = Double.Parse(AccelReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.SpeedSumBelowOneReiforcment = Double.Parse(SpeedSumBelowOneReiforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.TrackPositionReinforcment = Double.Parse(TrackPositionReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.OffTrackReinforcment = Double.Parse(OffTrackReinforcmentTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.TraveledDistanceReinforcment = Double.Parse(TraveledDistanceReinforcmentTextBox.Text, CultureInfo.CurrentCulture);

            Configuration.SpeedTolerance = Double.Parse(SpeedToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.AngleTolerance = Double.Parse(AngleToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.AccelTolerance = Double.Parse(AccelToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.BreakTolerance = Double.Parse(BreakToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.SteeringTolerance = Double.Parse(SteeringToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.FocusTolerance = Double.Parse(FocusToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.TrackTolerance = Double.Parse(TrackToleranceTectBox.Text, CultureInfo.CurrentCulture);
            Configuration.DistanceFromStartTolerance = Double.Parse(DistanceFromStartToleranceTextBox.Text, CultureInfo.CurrentCulture);
            Configuration.TrackPositionTolerance = Double.Parse(TrackPositionToleranceTextBox.Text, CultureInfo.CurrentCulture);

            Configuration.SpeedComparasionEnabled = SpeedComparasionEnabledCheckBox.Checked;
            Configuration.AngleComparasionEnabled = AngleComparasionEnabledCheckBox.Checked;
            Configuration.AccelComparasionEnabled = AccelComparasionEnabledCheckBox.Checked;
            Configuration.BreakComparasionEnabled = BreakComparasionEnabledCkechBox.Checked;
            Configuration.SteeringComparasionEnabled = SpeedComparasionEnabledCheckBox.Checked;
            Configuration.FocusComparasionEnabled = FocusComparasionEnabledCheckBox.Checked;
            Configuration.TrackComparasionEnabled = TrackComparasionEnabledCheckBox.Checked;
            Configuration.DistanceFromStartComparasionEnabled = DistanceFromStartComparasionEnabledCheckBox.Checked;
            Configuration.TrackPositionComparasionEnabled = TrackPositionComparasionEnabledCheckBox.Checked;
        }

        private void CancelButton_Click( Object sender, EventArgs e )
        {
            SaveButton.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}