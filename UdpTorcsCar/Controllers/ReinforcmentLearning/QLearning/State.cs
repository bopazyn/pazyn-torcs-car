﻿using System;
using System.Collections.Generic;
using System.Linq;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning
{
    public class State
    {
        public State()
        {
            GenerateMinimalPossibleActions();
        }

        public State(CarEfectorCollection effectors, CarSensorCollection sensors) : this()
        {
            Angle = sensors.Angle.Values[0];
            SpeedX = sensors.SpeedX.Values[0];
            SpeedY = sensors.SpeedY.Values[0];
            TrackPosition = sensors.TrackPosition.Values[0];

            Focus = new Single[sensors.Focus.SensorsCount];
            Array.Copy(sensors.Focus.Values, Focus, Focus.Length);

            Track = new Single[sensors.Track.SensorsCount];
            Array.Copy(sensors.Track.Values, Track, Track.Length);

            Accel = effectors.Accel.Value;
            Break = effectors.Break.Value;
            Steering = effectors.Steering.Value;
        }

        public Double Q { get; set; }
        public Action[] PossibleActions { get; set; }

        public Single Angle { get; set; }
        public Single SpeedX { get; set; }
        public Single SpeedY { get; set; }
        public Single[] Focus { get; set; }
        public Single[] Track { get; set; }

        public Single Accel { get; set; }
        public Single Break { get; set; }
        public Single Steering { get; set; }
        public Single TrackPosition { get; set; }


        private static Random RandomGenerator { get; } = new Random(DateTime.Now.Millisecond);

        public static Boolean Equals(State aState, State bState)
        {
            Configuration.Configuration configuration = Configuration.Configuration.Instance;

            if (Math.Abs(aState.TrackPosition - bState.TrackPosition) > configuration.TrackPositionTolerance && configuration.TrackPositionComparasionEnabled)
                return false;
            if (Math.Abs(aState.Angle - bState.Angle) > configuration.AngleTolerance && configuration.AngleComparasionEnabled)
                return false;
            if (Math.Abs(aState.SpeedX - bState.SpeedX) > configuration.SpeedTolerance && configuration.SpeedComparasionEnabled)
                return false;
            if (Math.Abs(aState.SpeedY - bState.SpeedY) > configuration.SpeedTolerance && configuration.SpeedComparasionEnabled)
                return false;
            if (Math.Abs(aState.Accel - bState.Accel) > configuration.AccelTolerance && configuration.AccelComparasionEnabled)
                return false;
            if (Math.Abs(aState.Break - bState.Break) > configuration.BreakTolerance && configuration.BreakComparasionEnabled)
                return false;
            if (Math.Abs(aState.Steering - bState.Steering) > configuration.SteeringTolerance && configuration.TrackComparasionEnabled)
                return false;

            if (aState.Focus.Where((t, i) => Math.Abs(t - bState.Focus[i]) > configuration.FocusTolerance).Any(x => configuration.FocusComparasionEnabled))
                return false;
            if (aState.Track.Where((t, i) => Math.Abs(t - bState.Track[i]) > configuration.TrackTolerance).Any(x => configuration.TrackComparasionEnabled))
                return false;

            return true;
        }

        private void GenerateMinimalPossibleActions()
        {
            List<Action> actions = new List<Action>
            {
                new Action(0, 0),
                new Action(0, 100),
                new Action(-300, 100),
                new Action(1000, 0),
                new Action(-1000, 0),
                new Action(300, 100)
            };
            PossibleActions = actions.ToArray();
        }

        private void GeneratePossibleActions()
        {
            List<Action> actions = new List<Action>();
            for (Int32 r = 4; r <= 10; r += 2)
                for (Int32 alfa = 0; alfa < 360; alfa += 30)
                {
                    Int32 x = (Int32)(r * Math.Sin(Math.PI / 180 * alfa) * 100);
                    Int32 y = (Int32)(r * Math.Cos(Math.PI / 180 * alfa) * 100);
                    actions.Add(new Action(x, y));
                }
            actions.Add(new Action(0, 0));
            PossibleActions = actions.ToArray();
        }

        public Action BoltzmanDistribution()
        {
            Configuration.Configuration configuration = Configuration.Configuration.Instance;

            List<Tuple<Double, Action>> actions = new List<Tuple<Double, Action>>();
            Double tmp = 0;
            Double expSum = PossibleActions.Sum(x => Math.Exp(x.QEstimated / configuration.Temperature));

            foreach (Action possibleAction in PossibleActions)
            {
                tmp += Math.Exp(possibleAction.QEstimated / configuration.Temperature);
                actions.Add(new Tuple<Double, Action>(tmp / expSum, possibleAction));
            }
            Double d = RandomGenerator.NextDouble();

            foreach (Tuple<Double, Action> tuple in actions)
                if (tuple.Item1 >= d)
                    return tuple.Item2;
            return null;
        }

        public Action EGreedy(Double e)
        {
            List<Tuple<Double, Action>> actions = new List<Tuple<Double, Action>>();
            Double max = PossibleActions.Max(x => x.QEstimated);
            Double tmp = 0;
            IEnumerable<Action> maxActions = PossibleActions.Where(x => Math.Abs(x.QEstimated - max) < 0.001);
            foreach (Action action in maxActions)
            {
                tmp += (1 - e) / maxActions.Count();
                actions.Add(new Tuple<Double, Action>(tmp, action));
            }
            IEnumerable<Action> restAction = PossibleActions.Where(x => Math.Abs(x.QEstimated - max) > 0.001);
            foreach (Action possibleAction in restAction)
            {
                tmp += e / restAction.Count();
                actions.Add(new Tuple<Double, Action>(tmp, possibleAction));
            }
            Double d = RandomGenerator.NextDouble();

            foreach (Tuple<Double, Action> tuple in actions)
                if (tuple.Item1 >= d)
                    return tuple.Item2;
            return null;
        }

        public Action RandomizeAction()
        {
            Int32 i = RandomGenerator.Next(0, PossibleActions.Length - 1);
            return PossibleActions[i];
        }
    }
}