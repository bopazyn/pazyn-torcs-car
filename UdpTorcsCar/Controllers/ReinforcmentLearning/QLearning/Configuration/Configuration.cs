﻿using System;
using System.IO;
using System.Web.Script.Serialization;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.Configuration
{
    public class Configuration
    {
        [ScriptIgnore] private const String Filename = "rewards.config";
        [ScriptIgnore] private static readonly Object SyncRoot = new Object();

        public Double Temperature { get; set; }
        public Double Gamma { get; set; }

        public Double AngleReinforcement { get; set; }
        public Double SpeedReinforcment { get; set; }
        public Double SpeedSumReinforcment { get; set; }
        public Double BreakReinforcment { get; set; }
        public Double AccelReinforcment { get; set; }
        public Double SpeedSumBelowOneReiforcment { get; set; }
        public Double TrackPositionReinforcment { get; set; }
        public Double OffTrackReinforcment { get; set; }
        public Double TraveledDistanceReinforcment { get; set; }


        public Double SpeedTolerance { get; set; }
        public Double AngleTolerance { get; set; }
        public Double AccelTolerance { get; set; }
        public Double BreakTolerance { get; set; }
        public Double SteeringTolerance { get; set; }
        public Double FocusTolerance { get; set; }
        public Double TrackTolerance { get; set; }
        public Double DistanceFromStartTolerance { get; set; }
        public Double TrackPositionTolerance { get; set; }


        public Boolean SpeedComparasionEnabled { get; set; }
        public Boolean AngleComparasionEnabled { get; set; }
        public Boolean AccelComparasionEnabled { get; set; }
        public Boolean BreakComparasionEnabled { get; set; }
        public Boolean SteeringComparasionEnabled { get; set; }
        public Boolean FocusComparasionEnabled { get; set; }
        public Boolean TrackComparasionEnabled { get; set; }
        public Boolean DistanceFromStartComparasionEnabled { get; set; }
        public Boolean TrackPositionComparasionEnabled { get; set; }

        [ScriptIgnore]
        private static JavaScriptSerializer JavaScriptSerializer { get; } = new JavaScriptSerializer();

        public static Configuration Instance { get; private set; }


        public static void Update( Configuration configuration )
        {
            Instance = configuration;
        }

        public void Save()
        {
            lock (SyncRoot)
            {
                String json = new JavaScriptSerializer().Serialize(this);
                File.WriteAllText(Filename, json);
            }
        }

        public static Configuration Load()
        {
            lock (SyncRoot)
            {
                return Instance ?? (Instance = File.Exists(Filename) ? ReadFromFile() : new Configuration());
            }
        }

        public static Configuration ReadFromFile()
        {
            return JavaScriptSerializer.Deserialize<Configuration>(File.ReadAllText(Filename));
        }
    }
}