﻿using System;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning
{
    public class Action
    {
        public Double QEstimated { get; set; }
        public Single Accel { get; private set; }
        public Single Break { get; private set; }
        public Single Steering { get; private set; }

        public Action()
        {
                
        }

        public Action(Int32 x, Int32 y)
        {
            Accel = y >= 0 ? (Single) y / 1000 : 0;
            Break = y <= 0 ? (Single) y / -1000 : 0;
            Steering = (Single) x / 1000;
        }

        public void UpdateEffectors( CarEfectorCollection effectors )
        {
            effectors.Accel.Value = Accel;
            effectors.Break.Value = Break;
            effectors.Steering.Value = Steering;
        }
    }
}
