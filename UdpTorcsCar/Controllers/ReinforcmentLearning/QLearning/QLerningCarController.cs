﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning
{
    public class QLerningCarController : ICarControllerable
    {
        public delegate Action GetManualControlActionDelegate();

        private const String Filename = "qlerning.json";

        private static readonly Double Gamma = Configuration.Configuration.Instance.Gamma;
        public GetManualControlActionDelegate GetManualControlActioCallback { get; set; }
        private StateCollection States { get; set; } = new StateCollection();
        private State LastState { get; set; }
        private Action LastAction { get; set; }

        public Boolean IsManualEnabled { get; set; }

        public void Drive( CarEfectorCollection effectors, CarSensorCollection sensors )
        {
            double beta = 0.1;
            try
            {
                if (LastState != null)
                {
                    Int32 r = new Reward(sensors, effectors).Calculate(LastState);
                    Double maxQ = LastState.PossibleActions.Max(x => x.QEstimated);

                    LastState.Q = (1 - beta) * LastState.Q + beta * (r + Gamma * maxQ);
                }
                State state = States.GetState(effectors, sensors);
                if (LastAction != null)
                    LastAction.QEstimated = state.Q;

                Action action = null;
                if (IsManualEnabled)
                {
                    Action tmp = GetManualControlActioCallback?.Invoke();
                    action = state.PossibleActions.FirstOrDefault(x => Math.Abs(x.Accel - tmp.Accel) < 0.1 && Math.Abs(x.Break - tmp.Break) < 0.1 && Math.Abs(x.Steering - tmp.Steering) < 0.1) ?? tmp;
                }

                if (!IsManualEnabled || action == null)
                    action = state.EGreedy(0.1);
                //action = state.BoltzmanDistribution();


                effectors.Gear.Value = 1;
                if (action == null)
                    action = LastAction;

                action?.UpdateEffectors(effectors);

                LastState = state;
                LastAction = action;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void SaveStates()
        {
            Task.Factory.StartNew(() =>
            {
                String data = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue }.Serialize(States);
                if (File.Exists(Filename))
                    File.Delete(Filename);
                File.WriteAllText(Filename, data);
            });
        }

        public void LoadStates()
        {
            Task.Factory.StartNew(() =>
            {
                if (File.Exists(Filename))
                    States = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue }.Deserialize<StateCollection>(File.ReadAllText(Filename));
            });
        }
    }
}