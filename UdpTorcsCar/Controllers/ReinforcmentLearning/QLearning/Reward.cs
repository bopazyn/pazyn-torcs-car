﻿using System;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning
{
    public class Reward
    {
        public Reward( CarSensorCollection sensors, CarEfectorCollection effectors )
        {
            Sensors = sensors;
            Effectors = effectors;
        }

        private CarEfectorCollection Effectors { get; }
        private CarSensorCollection Sensors { get; }

        public Int32 Calculate( State state )
        {
            Configuration.Configuration configuration = Configuration.Configuration.Instance;

            return (Int32) (
                configuration.TraveledDistanceReinforcment * Sensors.DistanceFromStart.Values[0] +
                configuration.AngleReinforcement * Math.Cos(state.Angle) +
                configuration.TrackPositionReinforcment * Math.Cos(state.TrackPosition) +
                (state.TrackPosition > 0.9 ? configuration.OffTrackReinforcment : 0) +
                (state.TrackPosition < -0.9 ? configuration.OffTrackReinforcment : 0) +
                configuration.SpeedReinforcment * (state.SpeedX * state.SpeedX) +
                configuration.SpeedReinforcment * (state.SpeedY * state.SpeedY) +
                configuration.BreakReinforcment * state.Break +
                configuration.AccelReinforcment * state.Accel +
                (state.SpeedX + state.SpeedY < 1 ? configuration.SpeedSumBelowOneReiforcment : 0)
            );
        }
    }
}