﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning;
using UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.Configuration;
using UdpTorcsCar.Controllers.ReinforcmentLearning.QLearning.UI;
using UdpTorcsCar.Core;
using UdpTorcsCar.Logging;

namespace UdpTorcsCar
{
    public partial class MainForm : Form, ILogger
    {
        public delegate void UpdateChartDelegate( CarSensorCollection sensors );

        public MainForm()
        {
            Configuration.Load();
            TorcsConnection torcsConnection = new TorcsConnection("127.0.0.1", 3001);

            CarController = new QLerningCarController();

            Car = new Car(torcsConnection, CarController, this);

            InitializeComponent();
            
            TrackChart.Legends.Clear();
        }

        private ICarControllerable CarController { get; }
        private Car Car { get; }
        private Boolean IsRestarting = false;

        public void Log( CarSensorCollection sensors, CarEfectorCollection effectors )
        {
            InvokeLogHandler(UpdateTrackChart, sensors);
            if (Math.Abs(sensors.TrackPosition.Values[0]) > 1 && !IsRestarting)
            {
                Task.Factory.StartNew(() =>
                {
                    IsRestarting = true;
                    Thread.Sleep(1000);
                    Car.StopRace();
                    Thread.Sleep(1000);
                    Car.StartRace();
                    IsRestarting = false;
                });
            }
        }

        private void StopRaceButton_Click( Object sender, EventArgs e )
        {
            Car.StopRace();
        }

        private void StartRaceButton_Click( Object sender, EventArgs e )
        {
            Car.StartRace();
        }

        private void UpdateTrackChart( CarSensorCollection sensors )
        {
            TrackChart.Series.Clear();
            for (Int32 i = 0; i < sensors.Track.Values.Length; i++)
            {
                Series series = TrackChart.Series.Add(i.ToString());
                series.Points.Add(sensors.Track.Values[i]);
            }
        }

        private void InvokeLogHandler( UpdateChartDelegate d, CarSensorCollection sensors )
        {
            if (TrackChart.InvokeRequired)
                Invoke(d, sensors);
            else
                d(sensors);
        }

        private void ConfigureRewardsButton_Click( Object sender, EventArgs e )
        {
            new ConfigurationForm().ShowDialog();
        }

        private void LoadQValuesButton_Click( Object sender, EventArgs e )
        {
            QLerningCarController qLerningCarController = CarController as QLerningCarController;
            qLerningCarController?.LoadStates();
        }

        private void SaveQValuesButton_Click( Object sender, EventArgs e )
        {
            QLerningCarController qLerningCarController = CarController as QLerningCarController;
            qLerningCarController?.SaveStates();
        }

        private void ManualControl_Click( Object sender, EventArgs e )
        {
            new ManualControlForm(CarController as QLerningCarController).ShowDialog();
        }
    }
}