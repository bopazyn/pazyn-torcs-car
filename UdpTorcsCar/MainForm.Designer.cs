﻿namespace UdpTorcsCar
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.StopRaceButton = new System.Windows.Forms.Button();
            this.TrackChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.StartRaceButton = new System.Windows.Forms.Button();
            this.ConfigureRewardsButton = new System.Windows.Forms.Button();
            this.LoadQValuesButton = new System.Windows.Forms.Button();
            this.SaveQValuesButton = new System.Windows.Forms.Button();
            this.ManualControl = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TrackChart)).BeginInit();
            this.SuspendLayout();
            // 
            // StopRaceButton
            // 
            this.StopRaceButton.Location = new System.Drawing.Point(12, 42);
            this.StopRaceButton.Name = "StopRaceButton";
            this.StopRaceButton.Size = new System.Drawing.Size(75, 23);
            this.StopRaceButton.TabIndex = 0;
            this.StopRaceButton.Text = "Stop Race";
            this.StopRaceButton.UseVisualStyleBackColor = true;
            this.StopRaceButton.Click += new System.EventHandler(this.StopRaceButton_Click);
            // 
            // TrackChart
            // 
            chartArea1.Name = "ChartArea1";
            this.TrackChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.TrackChart.Legends.Add(legend1);
            this.TrackChart.Location = new System.Drawing.Point(12, 283);
            this.TrackChart.Name = "TrackChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.TrackChart.Series.Add(series1);
            this.TrackChart.Size = new System.Drawing.Size(560, 166);
            this.TrackChart.TabIndex = 1;
            this.TrackChart.Text = "chart1";
            // 
            // StartRaceButton
            // 
            this.StartRaceButton.Location = new System.Drawing.Point(12, 13);
            this.StartRaceButton.Name = "StartRaceButton";
            this.StartRaceButton.Size = new System.Drawing.Size(75, 23);
            this.StartRaceButton.TabIndex = 2;
            this.StartRaceButton.Text = "Start Race";
            this.StartRaceButton.UseVisualStyleBackColor = true;
            this.StartRaceButton.Click += new System.EventHandler(this.StartRaceButton_Click);
            // 
            // ConfigureRewardsButton
            // 
            this.ConfigureRewardsButton.Location = new System.Drawing.Point(450, 13);
            this.ConfigureRewardsButton.Name = "ConfigureRewardsButton";
            this.ConfigureRewardsButton.Size = new System.Drawing.Size(122, 23);
            this.ConfigureRewardsButton.TabIndex = 3;
            this.ConfigureRewardsButton.Text = "Configure Rewards";
            this.ConfigureRewardsButton.UseVisualStyleBackColor = true;
            this.ConfigureRewardsButton.Click += new System.EventHandler(this.ConfigureRewardsButton_Click);
            // 
            // LoadQValuesButton
            // 
            this.LoadQValuesButton.Location = new System.Drawing.Point(450, 42);
            this.LoadQValuesButton.Name = "LoadQValuesButton";
            this.LoadQValuesButton.Size = new System.Drawing.Size(122, 23);
            this.LoadQValuesButton.TabIndex = 5;
            this.LoadQValuesButton.Text = "Load Q Values";
            this.LoadQValuesButton.UseVisualStyleBackColor = true;
            this.LoadQValuesButton.Click += new System.EventHandler(this.LoadQValuesButton_Click);
            // 
            // SaveQValuesButton
            // 
            this.SaveQValuesButton.Location = new System.Drawing.Point(450, 71);
            this.SaveQValuesButton.Name = "SaveQValuesButton";
            this.SaveQValuesButton.Size = new System.Drawing.Size(122, 23);
            this.SaveQValuesButton.TabIndex = 6;
            this.SaveQValuesButton.Text = "Save Q Values";
            this.SaveQValuesButton.UseVisualStyleBackColor = true;
            this.SaveQValuesButton.Click += new System.EventHandler(this.SaveQValuesButton_Click);
            // 
            // ManualControl
            // 
            this.ManualControl.Location = new System.Drawing.Point(322, 12);
            this.ManualControl.Name = "ManualControl";
            this.ManualControl.Size = new System.Drawing.Size(122, 23);
            this.ManualControl.TabIndex = 7;
            this.ManualControl.Text = "Manual Control";
            this.ManualControl.UseVisualStyleBackColor = true;
            this.ManualControl.Click += new System.EventHandler(this.ManualControl_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.ManualControl);
            this.Controls.Add(this.SaveQValuesButton);
            this.Controls.Add(this.LoadQValuesButton);
            this.Controls.Add(this.ConfigureRewardsButton);
            this.Controls.Add(this.StartRaceButton);
            this.Controls.Add(this.TrackChart);
            this.Controls.Add(this.StopRaceButton);
            this.Name = "MainForm";
            this.Text = "Torcs AI";
            ((System.ComponentModel.ISupportInitialize)(this.TrackChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StopRaceButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart TrackChart;
        private System.Windows.Forms.Button StartRaceButton;
        private System.Windows.Forms.Button ConfigureRewardsButton;
        private System.Windows.Forms.Button LoadQValuesButton;
        private System.Windows.Forms.Button SaveQValuesButton;
        private System.Windows.Forms.Button ManualControl;
    }
}