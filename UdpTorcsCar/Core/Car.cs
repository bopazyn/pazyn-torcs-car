﻿using System;
using System.Collections.Generic;
using UdpTorcsCar.Logging;

namespace UdpTorcsCar.Core
{
    public class Car
    {
        public Car( TorcsConnection connection, ICarControllerable controller )
        {
            TorcsConnection = connection;
            Controller = controller;
            TorcsConnection.OnDataReceived += HandleReceivedData;
        }

        public Car( TorcsConnection connection, ICarControllerable controller, params ILogger[] loggerCollection )
            : this(connection, controller)
        {
            LoggerCollection = loggerCollection;
        }

        private ICarControllerable Controller { get; }
        private IEnumerable<ILogger> LoggerCollection { get; }
        private CarSensorCollection Sensors { get; } = new CarSensorCollection();
        private CarEfectorCollection Effectors { get; } = new CarEfectorCollection();
        private TorcsConnection TorcsConnection { get; }


        private void HandleReceivedData( Object sedner, String data )
        {
            if (data.StartsWith("***"))
            {
                
            }
            else
            {
                Sensors.ReadCarState(data);
                Controller.Drive(Effectors, Sensors);
                TorcsConnection.Send(Effectors.ToString());
                if (LoggerCollection == null)
                    return;
                foreach (ILogger logger in LoggerCollection)
                    logger?.Log(Sensors, Effectors);
            }
           
        }

        public void StopRace()
        {
            Effectors.Meta.Value = 1;
            TorcsConnection.Send(Effectors.ToString());
            Effectors.Meta.Value = 0;
        }

        private void SendInitMessage()
        {
            TorcsConnection.Send("SCR(init -90 -60 -30 -20 -10 0 10 20 30 60 90)");
        }

        public void StartRace()
        {
            SendInitMessage();
            if (!TorcsConnection.IsRunning)
            {
                TorcsConnection.SetupTask();
            }
        }
    }
}