﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UdpTorcsCar.Core
{
    public class TorcsConnection
    {
        public const String IndetifiedCommand = "***identified***";
        public const String ShutdownCommand = "***shutdown***";
        public const String RestartCommand = "***restart***";

        public TorcsConnection( String hostname, Int32 port )
        {
            Hostname = hostname;
            Port = port;
            UdpClient = new UdpClient(Hostname, Port)
            {
                Client =
                {
                    ReceiveTimeout = 0
                }
            };
        }

        private String Hostname { get; }
        private Int32 Port { get; }
        private UdpClient UdpClient { get; }
        private Task ListeningTask { get; set; }
        private CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public event EventHandler<String> OnDataReceived;
        public Boolean IsRunning => ListeningTask != null && ListeningTask.Status == TaskStatus.Running;


        public void SetupTask()
        {
            CancellationTokenSource = new CancellationTokenSource();
            ListeningTask = Task.Factory.StartNew(() =>
            {
                while (!CancellationTokenSource.Token.IsCancellationRequested)
                {
                    try
                    {
                        String receivedString = Receive();
                        OnDataReceived?.Invoke(this, receivedString);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                CancellationTokenSource.Token.ThrowIfCancellationRequested();
               
            }, CancellationTokenSource.Token);
        }

        public void Send( String stringToSend )
        {
            Byte[] data = Encoding.ASCII.GetBytes(stringToSend);
            UdpClient.Send(data, data.Length);
        }

        private String Receive()
        {
            IPEndPoint remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            Byte[] data = UdpClient.Receive(ref remoteIpEndPoint);
            String tmp = Encoding.ASCII.GetString(data);
            if (tmp[tmp.Length - 1] == '\0')
                tmp = tmp.Substring(0, tmp.Length - 1);
            return tmp;
        }
    }
}