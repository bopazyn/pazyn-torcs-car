﻿using System;

namespace UdpTorcsCar.Core
{
    public class CarSensorCollection
    {
        private const Int32 FocusSensorsNumber = 5;
        private const Int32 TrackSensorsNumber = 19;
        private const Int32 OpponentsSensorsNumber = 36;

        public String LastReadState { get; private set; }

        public CarSensor<Single> Angle { get; private set; } = new CarSensor<Single>("angle");
        public CarSensor<Single> CurrentLapTime { get; private set; } = new CarSensor<Single>("curLapTime");
        public CarSensor<Single> LastLapTime { get; private set; } = new CarSensor<Single>("lastLapTime");
        public CarSensor<Single> Damage { get; private set; } = new CarSensor<Single>("damage");
        public CarSensor<Single> DistanceFromStart { get; private set; } = new CarSensor<Single>("distFromStart");
        public CarSensor<Single> DistanceRaced { get; private set; } = new CarSensor<Single>("distRaced");
        public CarSensor<Single> Focus { get; private set; } = new CarSensor<Single>("focus", FocusSensorsNumber);
        public CarSensor<Single> Fuel { get; private set; } = new CarSensor<Single>("fuel");
        public CarSensor<Int16> Gear { get; private set; } = new CarSensor<Int16>("gear");
        public CarSensor<Single> Opponents { get; private set; } = new CarSensor<Single>("opponents", OpponentsSensorsNumber);
        public CarSensor<Single> Track { get; private set; } = new CarSensor<Single>("track", TrackSensorsNumber);
        public CarSensor<Int16> RacePosition { get; private set; } = new CarSensor<Int16>("racePos");
        public CarSensor<Single> TrackPosition { get; private set; } = new CarSensor<Single>("trackPos");
        public CarSensor<Single> Rpm { get; private set; } = new CarSensor<Single>("rpm");
        public CarSensor<Single> SpeedX { get; private set; } = new CarSensor<Single>("speedX");
        public CarSensor<Single> SpeedY { get; private set; } = new CarSensor<Single>("speedY");
        public CarSensor<Single> SpeedZ { get; private set; } = new CarSensor<Single>("speedZ");
        public CarSensor<Single> Z { get; private set; } = new CarSensor<Single>("z");
        public CarSensor<Single> WheelsSpinVelocity { get; private set; } = new CarSensor<Single>("wheelSpinVel", 4);

        public void ReadCarState( String carState )
        {
            LastReadState = carState;

            Angle.Parse(carState);
            CurrentLapTime.Parse(carState);
            LastLapTime.Parse(carState);
            Damage.Parse(carState);
            DistanceFromStart.Parse(carState);
            DistanceRaced.Parse(carState);
            Focus.Parse(carState);
            Fuel.Parse(carState);
            Gear.Parse(carState);
            Opponents.Parse(carState);
            Track.Parse(carState);
            RacePosition.Parse(carState);
            TrackPosition.Parse(carState);
            Rpm.Parse(carState);
            SpeedX.Parse(carState);
            SpeedX.Parse(carState);
            SpeedY.Parse(carState);
            SpeedZ.Parse(carState);
            SpeedZ.Parse(carState);
            Z.Parse(carState);
            WheelsSpinVelocity.Parse(carState);
        }

        public override String ToString()
        {
            return LastReadState;
        }
    }
}