﻿using System;
using System.Globalization;
using System.Linq;

namespace UdpTorcsCar.Core
{
    public class CarSensor<T>
    {
        public T[] Values { get; private set; }
        public String TagName { get; private set; }
        public Int16 SensorsCount { get; private set; }

        public CarSensor( String tagName, Int16 sensorsCount = 1 )
        {
            TagName = tagName;
            Values = new T[sensorsCount];
            SensorsCount = sensorsCount;
        }

        public void Parse( String text )
        {
            if (text[0] != '(' || text[text.Length - 1] != ')')
                return;
            text = text.Substring(1, text.Length - 2);

            String[] values =
                text.Split(new[] { ")(" }, StringSplitOptions.RemoveEmptyEntries)
                    .FirstOrDefault(x => x.Contains(TagName))?.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (values == null)
                return;
            if (values.Length < SensorsCount + 1)
                return;
            for (Int32 i = 0; i < SensorsCount; i++)
            {
                Values[i] = (T)Convert.ChangeType(values[i + 1], typeof(T), new CultureInfo("en-US"));
            }
        }

        public override String ToString()
        {
            String tmp = $"({TagName}";
            for (Int32 i = 0; i < SensorsCount; i++)
            {
                tmp += $" {Values[i]}";
            }
            tmp += ")";
            return tmp;
        }

        public static Boolean Compare<Ti>( CarSensor<Ti> a, T[] values ) where Ti : IComparable
        {
            if (a.Values.Length != values.Length)
                throw new Exception();
            return !values.Where(( t, i ) => a.Values[i].CompareTo(t) != 0).Any();
        }
    }
}