﻿using System;

namespace UdpTorcsCar.Core
{
    public class CarEfectorCollection
    {
        public CarEfector<Single> Accel { get; private set; } = new CarEfector<Single>("accel", 0, 1);
        public CarEfector<Single> Break { get; private set; } = new CarEfector<Single>("brake", 0, 1);
        public CarEfector<Int16> Gear { get; private set; } = new CarEfector<Int16>("gear", -1, 7);
        public CarEfector<Single> Steering { get; private set; } = new CarEfector<Single>("steer", -1, 1);
        public CarEfector<Single> Clutch { get; private set; } = new CarEfector<Single>("clutch", 0, 1);
        public CarEfector<Byte> Meta { get; private set; } = new CarEfector<Byte>("meta", 0, 1);
        public CarEfector<Single> Focus { get; private set; } = new CarEfector<Single>("focus", -90, 90);

        public override String ToString()
        {
            return String.Empty + Accel + Break + Gear + Steering + Clutch + Meta + Focus;
        }
    }
}