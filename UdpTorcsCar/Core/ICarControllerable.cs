﻿namespace UdpTorcsCar.Core
{
    public interface ICarControllerable
    {
        void Drive( CarEfectorCollection effectors, CarSensorCollection sensors );
    }
}