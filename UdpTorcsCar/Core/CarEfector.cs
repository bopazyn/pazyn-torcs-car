﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace UdpTorcsCar.Core
{
    public class CarEfector<T> where T : IComparable
    {
        private T _value;

        public T Value
        {
            get { return _value; }
            set
            {
                if (Comparer<T>.Default.Compare(value, MinValue) < 0)
                    _value = MinValue;
                else if (Comparer<T>.Default.Compare(value, MaxValue) > 0)
                    _value = MaxValue;
                else
                {
                    _value = value;
                }
            }
        }

        public String TagName { get; private set; }
        public T MinValue { get; private set; }
        public T MaxValue { get; private set; }

        public CarEfector( String tagName, T min, T max )
        {
            TagName = tagName;
            MinValue = min;
            MaxValue = max;
        }

        public override String ToString()
        {
            if (typeof(T) == typeof(Single))
            {
                Single tmp = Convert.ToSingle(Value);
                return $"({TagName} {tmp.ToString(CultureInfo.InvariantCulture)})";
            }
            return $"({TagName} {Value})";
        }
    }
}