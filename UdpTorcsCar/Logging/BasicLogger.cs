﻿using System;
using System.IO;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Logging
{
    public class BasicLogger : ILogger
    {
        public BasicLogger( String path)
        {
            Path = path;
            StreamWriter = new StreamWriter(File.OpenWrite(Path));
        }

        public String Path { get; private set; }
        private StreamWriter StreamWriter { get; set; }

        public void Log( CarSensorCollection sensors, CarEfectorCollection effectors )
        {
            StreamWriter.WriteLine(sensors + "\t" + effectors);
        }
    }
}