﻿using System;
using UdpTorcsCar.Core;

namespace UdpTorcsCar.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void Log( CarSensorCollection sensors, CarEfectorCollection effectors )
        {
            Console.WriteLine(sensors + "\t" + effectors);
        }
    }
}