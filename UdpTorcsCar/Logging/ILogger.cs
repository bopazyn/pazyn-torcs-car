﻿using UdpTorcsCar.Core;

namespace UdpTorcsCar.Logging
{
    public interface ILogger
    {
        void Log( CarSensorCollection sensors, CarEfectorCollection effectors );
    }
}